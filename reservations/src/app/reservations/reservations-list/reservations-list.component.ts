import { Component, OnInit } from '@angular/core';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';


import { Reservation } from '../../models/reservation';
import { ReservationsListService } from './reservations-list.service';
import { ReservationDto } from 'src/app/dtos/reservation-dto';
import {ListDto} from '../../dtos/list-dto';
import {SortEnumReservation } from '../../models/enums/sort-enum-reservation';

@Component({
  selector: 'app-reservations-list',
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.css']
})
export class ReservationsListComponent implements OnInit {

  public reservations: ListDto<ReservationDto>;
  public selectedReservation?: Reservation;
  public selectedOrder: any;
  public emptyList: boolean;
  public order: string;
  public ascending: boolean;
  public pageIndex: number;
  public pageSize: number;

  constructor( private reservationsListService : ReservationsListService, config: NgbRatingConfig) {
    config.max = 5;
    config.readonly = true;
   }

  ngOnInit(): void {   
    this.emptyList = false;
    this.order ="ReservationId";
    this.selectedOrder ="-1";
    this.ascending = true;
    this.pageIndex = 1;
    this.pageSize =2;
    this.getReservations();
    this.reservations = new ListDto<ReservationDto>( new Array<ReservationDto>());

  }

  public onSelect(reservation: Reservation): void {
    this.selectedReservation = reservation;
  }

  public updateOrder(): void{
   switch (Number.parseInt(this.selectedOrder))
   {
     case SortEnumReservation.alphabeticAsc:
     {
      this.order ="Description";
      this.ascending = true;
       break;
     }
     case SortEnumReservation.alphabeticDesc:
     {      
       this.order ="Description";
       this.ascending = false;
       break;
     }
     case SortEnumReservation.dateAsc:
    {
      this.order ="Date";
      this.ascending = true;
      break;
    }
    case SortEnumReservation.dateDes:
    {
      this.order ="Date";
      this.ascending = false;
      break;
    }
    case SortEnumReservation.rankingAsc:
    {
      this.order ="Ranking";
      this.ascending = true;
      break;
    }
    case SortEnumReservation.rankingDesc:
    {
      this.order ="Ranking";
      this.ascending = false;
      break;
    }
    default:
    {       
      this.order ="ReservationId";
      this.ascending = true;
      break;         
    }
   }   
   this.getReservations();
  }

  public getReservations(): void{
    this.reservationsListService.listReservations(this.order, this.ascending, this.pageIndex, this.pageSize)  
    .subscribe((reservations) => {
      this.reservations = reservations;
      this.emptyList =  this.reservations.count === 0;
    },(error) =>  {
      this.emptyList = true;
    console.log(error);
    });
  }

 

}
