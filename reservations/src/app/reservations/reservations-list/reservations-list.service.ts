import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';
import {ReservationDto} from '../../dtos/reservation-dto';
import {ListDto} from '../../dtos/list-dto';

@Injectable({
  providedIn: 'root'
})
export class ReservationsListService {

  constructor(private http: HttpClient) { }

  public listReservations(order: string, ascending: boolean , pageIndex: number,pageSize:number ): Observable<ListDto<ReservationDto>> {    
    const paramsObj = new HttpParams().set('pageIndex', pageIndex.toString()).set('pageSize', pageSize.toString()).set('order', order).set('ascending',ascending.toString());
    return this.http.get<ListDto<ReservationDto>>(`${environment.apiUrl}reservations`,{ params: paramsObj} );     

  }
}
