import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';
import {ReservationDto} from '../../dtos/reservation-dto';

@Injectable({
  providedIn: 'root'
})
export class ReservationsDetailService {

  constructor(private http: HttpClient) { }

  public getReservation(reservationId: number ): Observable<ReservationDto> {
    return this.http.get<ReservationDto>(`${environment.apiUrl}reservations/${reservationId}`);    
  }
}
