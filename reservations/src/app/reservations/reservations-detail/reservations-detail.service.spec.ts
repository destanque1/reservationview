import { TestBed } from '@angular/core/testing';

import { ReservationsDetailService } from './reservations-detail.service';

describe('ReservationsDetailService', () => {
  let service: ReservationsDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationsDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
