import { Component, OnInit , Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ReservationsDetailService } from './reservations-detail.service';
import { Reservation } from '../../models/reservation';

@Component({
  selector: 'app-reservations-detail',
  templateUrl: './reservations-detail.component.html',
  styleUrls: ['./reservations-detail.component.css']
})
export class ReservationsDetailComponent implements OnInit {
 
  public  reservation: Reservation;

  constructor(private route: ActivatedRoute, private location: Location, private reservationsDetailService : ReservationsDetailService) { }

  ngOnInit(): void {
   
  }

  public getReservation(): void{
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.reservationsDetailService.getReservation(id)  
    .subscribe((reservation) => {
      this.reservation= reservation;
    },(error) =>  {
    console.log(error);
    });
  }

  public goBack(): void {
    this.location.back();
  }

}
