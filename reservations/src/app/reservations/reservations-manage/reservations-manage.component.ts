import { Component, OnInit } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { ToastService } from 'ng-uikit-pro-standard';
import * as _ from 'underscore';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';

import {ContactTypeDto} from '../../dtos/contact-type-dto';
import {ContactDto} from '../../dtos/contact-dto';
import {Contact} from '../../models/contact';
import {ListDto} from '../../dtos/list-dto';
import {Reservation} from '../../models/reservation';
import {ReservationsManageService} from './reservations-manage.service';

@Component({
  selector: 'app-reservations-manage',
  templateUrl: './reservations-manage.component.html',
  styleUrls: ['./reservations-manage.component.css']
})
export class ReservationsManageComponent implements OnInit {

  public order: string;
  public ascending: boolean;
  public pageIndex: number;
  public pageSize: number;
  public contacts: ListDto<ContactDto>;
  public contact: Contact;
  public contactsTypes: ListDto<ContactTypeDto>; 
  public reservation: Reservation;  
  public reservationForm: FormGroup;
  public disableSave : boolean;
  private controlsValidationConfig: Object;
  private contactId: number;
  private reservationId: number;  
  private editReservation : boolean; 
 

  constructor(private reservationsManageService : ReservationsManageService,  private formBuilder: FormBuilder, 
     private router: Router,private route: ActivatedRoute, private toastrService: ToastService,  private translate: TranslateService) { }

  ngOnInit(): void {
    this.reservationId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    this.order ="";  
    this.ascending = true;
    this.pageIndex = 1;
    this.pageSize = 200;
    this.contactsTypes = new ListDto<ContactTypeDto>( new Array<ContactTypeDto>());
    this.contacts = new ListDto<ContactDto>( new Array<ContactDto>());
    this.contact = new Contact();
    this.reservation = new Reservation();       
    this.disableSave = false;
    this.editReservation =  !_.isNaN(this.reservationId);
    if(this.editReservation)
      this.getReservationAndContact();
    else{      
      this.getContactsAndTypes(); 
    }   

    this.reservationForm = this.formBuilder.group({
      ContactName:  [{ value: '', disabled: this.editReservation }],
      ContactType:  [{ value: null, disabled: this.editReservation }  ],
      ContactPhone:  [{ value: '', disabled: this.editReservation }],
      ContactbirthDate:  [{ value: undefined, disabled: this.editReservation }],
      ReservationDate:  [{ value: undefined, disabled: false }],
      ResDescription:  [{ value: '', disabled: false }]     
  });

  this.controlsValidationConfig = this.buildControlsValidation();

  }

  get f() { return this.reservationForm.controls; }

  private buildControlsValidation(): Object {
    const validations = {
      ContactName: [Validators.required],
      ContactType: [Validators.required],
      ContactbirthDate: [Validators.required],
      ReservationDate: [Validators.required],
      ResDescription: [Validators.required],
      ContactPhone: [Validators.pattern('^[0-9]+$'), Validators.minLength(8), Validators.maxLength(16)
    ],
     
    };
    return validations;
  }

  private getContactsAndTypes(): void{
    forkJoin([
      this.reservationsManageService.listContacts(this.order, this.ascending, this.pageIndex, this.pageSize),
      this.reservationsManageService.listContactTypes(this.order, this.ascending, this.pageIndex, this.pageSize) 
    ]).subscribe((data: any) => {
      this.contacts = data[0];   
      this.contactsTypes = data[1];     
    }, (error) =>  {      
      console.log(error);
      }, () => {
      console.log('end')
    });    
  }

  private getReservationAndContact(): void{
    forkJoin([
      this.reservationsManageService.listContacts(this.order, this.ascending, this.pageIndex, this.pageSize),
      this.reservationsManageService.listContactTypes(this.order, this.ascending, this.pageIndex, this.pageSize),
      this.reservationsManageService.getReservations(this.reservationId)
    ]).subscribe((data: any) => {
      this.contacts = data[0];   
      this.contactsTypes = data[1];  
      this.loadData(data[2]);      
    }, (error) =>  {      
      console.log(error);
      }, () => {
      console.log('end')
    });    


  }

  private loadData(reservation: Reservation): void
  {
    //Reservation
    const date = new Date( reservation.date);      
    const dateReservation = {year : date.getFullYear(), month : date.getMonth() + 1, day : date.getDate()};
    this.f.ReservationDate.setValue(dateReservation);
    this.f.ResDescription.setValue(reservation.description); 
    this.reservation.contactId = reservation.contactId;;
    this.reservation.reservationId = reservation.reservationId;

    //Contact   
    const result = this.contacts.list.find(x=> x.contactId == this.reservation.contactId);
    if(result) {
      this.f.ContactName.setValue(result.name);
      this.f.ContactType.setValue(result.type);
      let dateBirth = new Date( result.birthDate);      
      let dateContact = {year : dateBirth.getFullYear(), month : dateBirth.getMonth() + 1, day : dateBirth.getDate()};
      this.f.ContactbirthDate.setValue(dateContact);
      this.f.ContactPhone.setValue(result.phone);
    }

  }

  private setControlValidation(controlKey?: string): void {
      
     //Others
    _.each(this.f, (value, controlKey) => {
      if (this.controlsValidationConfig.hasOwnProperty(controlKey)) {
        this.f[controlKey].setValidators(this.controlsValidationConfig[controlKey]);
        this.f[controlKey].updateValueAndValidity();      
      }
      if (this.f[controlKey].invalid) {
        this.f[controlKey].markAsTouched();
      }
    });
      //Validating dates 
    if(typeof this.f.ContactbirthDate.value !== 'object' ) {
      this.f.ContactbirthDate.setErrors({'invalid': true});
      return;
    }
    if(typeof this.f.ReservationDate.value !== 'object'){
      this.f.ReservationDate.setErrors({'invalid': true});
      return;
    }

    const dateNow = new Date();
    const contactbirthDate = this.BuiltDate(this.f.ContactbirthDate.value);
    const reservationDate = this.BuiltDate(this.f.ReservationDate.value);
    if(contactbirthDate > dateNow){
      this.f.ContactbirthDate.setErrors({'invalid': true});
      return;
    }
    if(reservationDate <  dateNow)  {
      this.f.ReservationDate.setErrors({'invalid': true});
      return;
    }

  }

  private SaveReservation(): void { 
     //Reservation
    this.reservation.date = this.BuiltDate(this.f.ReservationDate.value);
    this.reservation.description = this.f.ResDescription.value;
    this.reservation.contactId = this.contactId;;  
    this.reservationsManageService.saveReservations( this.reservation).subscribe((res) => {
      console.log('success');
      this.showSuccess();
      this.router.navigate(['/reservations/list']);
    },(error) =>  {      
    console.log(error);
    let msg = _.first(error.error.Errors);
    if(msg)
      msg = msg.Message
    else
      msg = '';
    this.showError(msg);
    });   
} 

private EditReservation(): void { 
  //Reservation
 this.reservation.date = this.BuiltDate(this.f.ReservationDate.value);
 this.reservation.description = this.f.ResDescription.value;
 this.reservationsManageService.updateReservations(this.reservation.reservationId, this.reservation).subscribe((res) => {
   console.log('success');
   this.showSuccess();
   this.router.navigate(['/reservations/list']);
 },(error) =>  {      
 console.log(error);
 let msg = _.first(error.error.Errors).Message;
 this.showError(msg);
 });   
} 

  private BuiltDate(date: NgbDateStruct): Date {     
      return new Date(date.year, date.month-1, date.day)
  } 

  private showError(error: string) : void {
    this.disableSave = false; 
    let msg =  error === '' ? this.translate.instant('general.errors') : error;
    this.toastrService.error(msg);
  }

  private showSuccess() : void{   
    this.toastrService.success( this.translate.instant('reservations.successCreate'));
  }

  public Submit(): void {   
    this.disableSave = true;
    this.setControlValidation();
    if (this.reservationForm.invalid) 
    {
      this.disableSave = false;   
      return;
    }

    if(this.editReservation){
      this.EditReservation();
    }
    else{
     //Contact   
      const result = this.contactsTypes.list.find(x=> x.description.toLocaleLowerCase() ==  this.f.ContactType.value.toLocaleLowerCase()); 
      this.contact.contactTypeId = result.contactTypeId;
      this.contact.phone = this.f.ContactPhone.value !== '' ? this.f.ContactPhone.value : null;     
      this.contact.name =  this.f.ContactName.value;     
      this.contact.birthDate = this.BuiltDate(this.f.ContactbirthDate.value)
     
      if(this.contactId <=0)
      { 
        //Save contact
        this.reservationsManageService.saveContacts( this.contact).subscribe((contact) => {
          console.log('success');
          this.contactId = contact.contactId;
          this.SaveReservation();         
        },(error) =>  {      
        console.log(error);       
        let msg = _.first(error.error.Errors);
        if(msg)
         msg = msg.Message
        else
          msg = '';
        this.showError(msg);
        });
      }
      else{
        //Update contact
        this.reservationsManageService.updateContacts( this.contactId, this.contact).subscribe(() => {
          console.log('success');         
          this.SaveReservation();         
        },(error) =>  {      
        console.log(error);      
        let msg = _.first(error.error.Errors);
        if(msg)
          msg = msg.Message
        else
          msg = '';
        this.showError(msg);
        });
      
      }
    }
  } 

    
  public search(): void {
    
    const result = this.contacts.list.find(x=> x.name.toLocaleLowerCase() == this.f.ContactName.value.toLocaleLowerCase());    
    if(result)
    {      
      let date = new Date( result.birthDate);      
      let birthDate = {year : date.getFullYear(), month : date.getMonth() + 1, day : date.getDate()};  
      this.contactId =  result.contactId; 
      //Form
      this.f.ContactName.setValue(result.name);
      this.f.ContactType.setValue(result.type);
      this.f.ContactbirthDate.setValue(birthDate);
      this.f.ContactPhone.setValue(result.phone);

    }
    else
    {
      this.contactId =  0; 
     //Form    
     this.f.ContactType.setValue(null);
     this.f.ContactbirthDate.setValue(null);
     this.f.ContactPhone.setValue('');
    }
    
  }


}
