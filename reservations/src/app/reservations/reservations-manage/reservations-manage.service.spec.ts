import { TestBed } from '@angular/core/testing';

import { ReservationsManageService } from './reservations-manage.service';

describe('ReservationsManageService', () => {
  let service: ReservationsManageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationsManageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
