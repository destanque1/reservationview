import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { ToolbarService, LinkService, ImageService, HtmlEditorService } from '@syncfusion/ej2-angular-richtexteditor';
import {ToastService} from 'ng-uikit-pro-standard'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]  
})
export class AppComponent {
  title = 'Reservations';
  constructor(private translate: TranslateService, private toast: ToastService) {
    translate.setDefaultLang('en');
}
  public useLanguage(language: string): void {
    this.translate.use(language);
  }
}
