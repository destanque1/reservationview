import { TestBed } from '@angular/core/testing';

import { ContactsManageService } from './contacts-manage.service';

describe('ContactsManageService', () => {
  let service: ContactsManageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactsManageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
