import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsManageComponent } from './contacts-manage.component';

describe('ContactsManageComponent', () => {
  let component: ContactsManageComponent;
  let fixture: ComponentFixture<ContactsManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactsManageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
