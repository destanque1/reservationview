import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import {ReservationDto} from '../../dtos/reservation-dto';
import {ContactTypeDto} from '../../dtos/contact-type-dto';
import {ContactDto} from '../../dtos/contact-dto';
import {Reservation} from '../../models/reservation';
import {Contact} from '../../models/contact';

import {ListDto} from '../../dtos/list-dto';

@Injectable({
  providedIn: 'root'
})
export class ContactsManageService {

  constructor(private http: HttpClient) { }

  public listContactTypes(order: string, ascending: boolean , pageIndex: number,pageSize:number ): Observable<ListDto<ContactTypeDto>> {    
    const paramsObj = new HttpParams().set('pageIndex', pageIndex.toString()).set('pageSize', pageSize.toString()).set('order', order).set('ascending',ascending.toString());
    return this.http.get<ListDto<ContactTypeDto>>(`${environment.apiUrl}contactTypes`,{ params: paramsObj} );     
  }
  
  public saveContacts(contact : Contact ): Observable<Contact> {    
    return this.http.post<Contact>(`${environment.apiUrl}contacts`,  contact );  
  }

  public updateContacts(contactId : number, contact : Contact ): Observable<Contact> {    
    return this.http.put<Contact>(`${environment.apiUrl}contacts/${contactId}`,  contact );  
  }

  public getContact(contactId : number): Observable<ContactDto> {    
    return this.http.get<ContactDto>(`${environment.apiUrl}contacts/${contactId}`);  
  }

  public listContacts(order: string, ascending: boolean , pageIndex: number,pageSize:number ): Observable<ListDto<ContactDto>> {    
    const paramsObj = new HttpParams().set('pageIndex', pageIndex.toString()).set('pageSize', pageSize.toString()).set('order', order).set('ascending',ascending.toString());
    return this.http.get<ListDto<ContactDto>>(`${environment.apiUrl}contacts`,{ params: paramsObj} );     
  }
  }
