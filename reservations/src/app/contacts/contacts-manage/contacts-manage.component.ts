import { Component, OnInit } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { ToastService } from 'ng-uikit-pro-standard';
import * as _ from 'underscore';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';

import {ContactTypeDto} from '../../dtos/contact-type-dto';
import {ContactDto} from '../../dtos/contact-dto';
import {Contact} from '../../models/contact';
import {ListDto} from '../../dtos/list-dto';
import {ContactsManageService} from './contacts-manage.service';

@Component({
  selector: 'app-contacts-manage',
  templateUrl: './contacts-manage.component.html',
  styleUrls: ['./contacts-manage.component.css']
})
export class ContactsManageComponent implements OnInit {

  public order: string;
  public ascending: boolean;
  public pageIndex: number;
  public pageSize: number;
  public contacts: ListDto<ContactDto>;
  public contact: Contact;
  public contactsTypes: ListDto<ContactTypeDto>;   
  public contactsForm: FormGroup;
  public disableSave : boolean;
  private controlsValidationConfig: Object;
  private contactId: number;
  private editContact: boolean; 
 

  constructor(private contactsManageService : ContactsManageService,  private formBuilder: FormBuilder, 
     private router: Router,private route: ActivatedRoute, private toastrService: ToastService,  private translate: TranslateService) { }

  ngOnInit(): void {
    this.contactId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    this.order ="";  
    this.ascending = true;
    this.pageIndex = 1;
    this.pageSize = 200;
    this.contactsTypes = new ListDto<ContactTypeDto>( new Array<ContactTypeDto>());
    this.contacts = new ListDto<ContactDto>( new Array<ContactDto>());
    this.contact = new Contact();
       
    this.disableSave = false;
    this.editContact =  !_.isNaN(this.contactId);
    if(this.contactId)
      this.getContactAndTypes();
    else{      
      this.getContactTypes(); 
    }   

    this.contactsForm = this.formBuilder.group({
      ContactName:  [{ value: '', disabled: false }],
      ContactType:  [{ value: null, disabled: false }  ],
      ContactPhone:  [{ value: '', disabled: false }],
      ContactbirthDate:  [{ value: undefined, disabled: false }]       
  });

  this.controlsValidationConfig = this.buildControlsValidation();

  }

  get f() { return this.contactsForm.controls; }

  private buildControlsValidation(): Object {
    const validations = {
      ContactName: [Validators.required],
      ContactType: [Validators.required],
      ContactbirthDate: [Validators.required],     
      ContactPhone: [Validators.pattern('^[0-9]+$'), Validators.minLength(8), Validators.maxLength(16)
    ],
     
    };
    return validations;
  }

  private getContactAndTypes(): void{
    forkJoin([
      this.contactsManageService.getContact(this.contactId),
      this.contactsManageService.listContactTypes(this.order, this.ascending, this.pageIndex, this.pageSize) 
    ]).subscribe((data: any) => {
      this.contacts = data[0];   
      this.contactsTypes = data[1];   
      this.loadData(data[0]);  
    }, (error) =>  {      
      console.log(error);
      }, () => {
      console.log('end')
    });    
  }

  private getContactTypes(): void{
    this.contactsManageService.listContactTypes(this.order, this.ascending, this.pageIndex, this.pageSize).subscribe((types) => {
      this.contactsTypes = types;     
    },(error) =>  {      
    console.log(error);  
    });   
  }  

  private loadData(contact: ContactDto): void
  { 
    //Contact 
    if(contact) {
      this.f.ContactName.setValue(contact.name);
      this.f.ContactType.setValue(contact.type);
      let dateBirth = new Date( contact.birthDate);      
      let dateContact = {year : dateBirth.getFullYear(), month : dateBirth.getMonth() + 1, day : dateBirth.getDate()};
      this.f.ContactbirthDate.setValue(dateContact);
      this.f.ContactPhone.setValue(contact.phone);
    }

  }

  private setControlValidation(controlKey?: string): void {
      
     //Others
    _.each(this.f, (value, controlKey) => {
      if (this.controlsValidationConfig.hasOwnProperty(controlKey)) {
        this.f[controlKey].setValidators(this.controlsValidationConfig[controlKey]);
        this.f[controlKey].updateValueAndValidity();      
      }
      if (this.f[controlKey].invalid) {
        this.f[controlKey].markAsTouched();
      }
    });
      //Validating dates 
    if(typeof this.f.ContactbirthDate.value !== 'object' ) {
      this.f.ContactbirthDate.setErrors({'invalid': true});
      return;
    }   

    const dateNow = new Date();
    const contactbirthDate = this.BuiltDate(this.f.ContactbirthDate.value);   
    if(contactbirthDate > dateNow){
      this.f.ContactbirthDate.setErrors({'invalid': true});
      return;
    }   

  }

  private BuiltDate(date: NgbDateStruct): Date {     
      return new Date(date.year, date.month-1, date.day)
  } 

  private showError(error: string) : void {
    this.disableSave = false; 
    let msg =  error === '' ? this.translate.instant('general.errors') : error;
    this.toastrService.error(msg);
  }

  private showSuccess() : void{   
    this.toastrService.success( this.translate.instant('reservations.successCreate'));
  }

  public Submit(): void {   
    this.disableSave = true;
    this.setControlValidation();
    if (this.contactsForm.invalid) 
    {
      this.disableSave = false;   
      return;
    }

    //Contact  
    const result = this.contactsTypes.list.find(x=> x.description.toLocaleLowerCase() ==  this.f.ContactType.value.toLocaleLowerCase()); 
    this.contact.contactTypeId = result.contactTypeId;
    this.contact.phone = this.f.ContactPhone.value !== '' ? this.f.ContactPhone.value : null;     
    this.contact.name =  this.f.ContactName.value;     
    this.contact.birthDate = this.BuiltDate(this.f.ContactbirthDate.value)     

    if(this.editContact){
      //Update contact
      this.contact.contactId = this.contactId;  
      this.contactsManageService.updateContacts( this.contact.contactId, this.contact).subscribe((contact) => {
        console.log('success');
        this.showSuccess();
        this.router.navigate(['/contacts/list']);                
      },(error) =>  {      
      console.log(error);       
      let msg = _.first(error.error.Errors);
      if(msg)
        msg = msg.Message
      else
        msg = '';
      this.showError(msg);
      });    
    }
    else{
      
      //Save contact
      this.contact.contactId = 0;  
      this.contactsManageService.saveContacts( this.contact).subscribe((contact) => {
        console.log('success');
        this.showSuccess();
        this.router.navigate(['/contacts/list']);                
      },(error) =>  {      
      console.log(error);       
      let msg = _.first(error.error.Errors);
      if(msg)
        msg = msg.Message
      else
        msg = '';
      this.showError(msg);
      });    
     
    }
  } 

    
  

}
