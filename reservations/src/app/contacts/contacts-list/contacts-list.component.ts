import { Component, OnInit } from '@angular/core';
import {NgbRatingConfig, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'ng-uikit-pro-standard';
import * as _ from 'underscore';
import { TranslateService } from '@ngx-translate/core';


import { ContactsDeleteModalComponent } from '../contacts-delete-modal/contacts-delete-modal.component';
import { ContactsListService } from './contacts-list.service';
import {ListDto} from '../../dtos/list-dto';
import {SortEnumContact } from '../../models/enums/sort-enum-contact';
import { ContactDto } from 'src/app/dtos/contact-dto';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {
  public contacts: ListDto<ContactDto>;
  public selectedOrder: any;
  public emptyList: boolean;
  public order: string;
  public ascending: boolean;
  public pageIndex: number;
  public pageSize: number;
  private deleteModal :NgbModalRef;

  constructor( private contactsListService : ContactsListService, config: NgbRatingConfig,
     private modalService: NgbModal, private toastrService: ToastService, private translate: TranslateService) {
    config.max = 5;
    config.readonly = true;
   }

  ngOnInit(): void {   
    this.emptyList = false;
    this.order ="ContactId";
    this.selectedOrder ="-1";
    this.ascending = true;
    this.pageIndex = 1;
    this.pageSize =2;
    this.getContacts();
    this.contacts = new ListDto<ContactDto>( new Array<ContactDto>());

  }

  public updateOrder(): void{
   switch (Number.parseInt(this.selectedOrder))
   {
     case SortEnumContact.alphabeticAsc:
     {
      this.order ="Name";
      this.ascending = true;
       break;
     }
     case SortEnumContact.alphabeticDesc:
     {      
       this.order ="Name";
       this.ascending = false;
       break;
     }
     case SortEnumContact.dateAsc:
    {
      this.order ="BirthDate";
      this.ascending = true;
      break;
    }
    case SortEnumContact.dateDes:
    {
      this.order ="BirthDate";
      this.ascending = false;
      break;
    }   
    default:
    {       
      this.order ="ReservationId";
      this.ascending = true;
      break;         
    }
   }   
   this.getContacts();
  }

  public getContacts(): void{
    this.contactsListService.listContacts(this.order, this.ascending, this.pageIndex, this.pageSize)  
    .subscribe((contacts) => {
      this.contacts = contacts;
      this.emptyList =  this.contacts.count === 0;
    },(error) =>  {
      this.emptyList = true;
    console.log(error);
    });
  }

  public showModalDelete(contactId: number): void{
    this.deleteModal = this.modalService.open(ContactsDeleteModalComponent,
      { size: 'md', backdrop: 'static', keyboard: false, centered: true });
            this.deleteModal.result.then(() => {           
    this.contactsListService.deleteContacts(contactId).subscribe(() => {
      this.showSuccess();
      this.getContacts();
    },(error: any) =>  {       
      let msg = _.first(error.error.Errors);
      if(msg)
        msg = msg.Message
      else
        msg = '';
      this.showError(msg);
    });
    }, (error: any) => {
        console.log(error);
    });
  }

  private showError(error: string) : void {  
    let msg =  error === '' ? this.translate.instant('general.errors') : error;
    this.toastrService.error(msg);
  }

  private showSuccess() : void{   
    this.toastrService.success( this.translate.instant('contacts.successDelete'));
  }

}
