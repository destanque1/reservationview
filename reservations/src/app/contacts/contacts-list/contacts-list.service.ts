import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';
import {ListDto} from '../../dtos/list-dto';
import { ContactDto } from 'src/app/dtos/contact-dto';

@Injectable({
  providedIn: 'root'
})
export class ContactsListService {

  constructor(private http: HttpClient) { }

  public listContacts(order: string, ascending: boolean , pageIndex: number,pageSize:number ): Observable<ListDto<ContactDto>> {    
    const paramsObj = new HttpParams().set('pageIndex', pageIndex.toString()).set('pageSize', pageSize.toString()).set('order', order).set('ascending',ascending.toString());
    return this.http.get<ListDto<ContactDto>>(`${environment.apiUrl}contacts`,{ params: paramsObj} );     

  }

  public deleteContacts(contactId: number): Observable<Object> {   
    return this.http.request('DELETE', `${environment.apiUrl}contacts/${contactId}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })      
    });
  }
}
