import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsDeleteModalComponent } from './contacts-delete-modal.component';

describe('ContactsDeleteModalComponent', () => {
  let component: ContactsDeleteModalComponent;
  let fixture: ComponentFixture<ContactsDeleteModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactsDeleteModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
