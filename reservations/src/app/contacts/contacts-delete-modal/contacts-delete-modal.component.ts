import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contacts-delete-modal',
  templateUrl: './contacts-delete-modal.component.html',
  styleUrls: ['./contacts-delete-modal.component.css']
})
export class ContactsDeleteModalComponent implements OnInit {

  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
