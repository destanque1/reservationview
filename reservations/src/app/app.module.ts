import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';
import { ToastModule } from 'ng-uikit-pro-standard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReservationsListComponent } from './reservations/reservations-list/reservations-list.component';
import { ReservationsManageComponent } from './reservations/reservations-manage/reservations-manage.component';
import { ReservationsDetailComponent } from './reservations/reservations-detail/reservations-detail.component';
import { ContactsListComponent } from './contacts/contacts-list/contacts-list.component';
import { ContactsManageComponent } from './contacts/contacts-manage/contacts-manage.component';
import { ContactsDetailComponent } from './contacts/contacts-detail/contacts-detail.component';
import { ContactsDeleteModalComponent } from './contacts/contacts-delete-modal/contacts-delete-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    ReservationsListComponent,
    ReservationsManageComponent,
    ReservationsDetailComponent,
    ContactsListComponent,
    ContactsManageComponent,
    ContactsDetailComponent,
    ContactsDeleteModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
    NgbModule,
    RichTextEditorModule,   
    ReactiveFormsModule,
    ToastModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

