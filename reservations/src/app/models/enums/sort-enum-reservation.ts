export enum SortEnumReservation {
   dateAsc,
   dateDes,
   alphabeticAsc,
   alphabeticDesc,
   rankingAsc,
   rankingDesc
};