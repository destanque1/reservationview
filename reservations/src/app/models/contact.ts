export class Contact {
    public contactId: number;
    public name: string;
    public phone: string;
    public contactTypeId: number;   
    public birthDate: Date;
  }