export class Reservation {
    public reservationId: number;
    public description: string;
    public ranking: number;
    public date: Date;
    public contactId: number;
    public statusId: number;
  }