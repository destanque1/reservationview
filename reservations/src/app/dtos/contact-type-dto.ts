export class ContactTypeDto {
    public contactTypeId: number;
    public code: string;
    public description: string;  
  }