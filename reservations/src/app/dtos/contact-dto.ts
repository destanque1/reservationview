export class ContactDto {
    public contactId: number;
    public name: string;
    public phone: string;
    public date: Date;
    public type: string;
    public birthDate: Date;
  }