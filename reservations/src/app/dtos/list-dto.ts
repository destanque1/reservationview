export class ListDto <T> {
  public count: number;
  public list: Array<T>;

  constructor(data ? : any) {
    this.count = data && data.count ? data.count : 0;
    this.list = data && data.list ? data.list : [];
  }
}