import "./single-error"
import { SingleError } from './single-error';

export class ErrorResponse {
    public Code: number;
    public Errors: Array<SingleError>;   
  }