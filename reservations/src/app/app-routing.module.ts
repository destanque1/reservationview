import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { ReservationsListComponent } from './reservations/reservations-list/reservations-list.component';
import { ReservationsDetailComponent } from './reservations/reservations-detail/reservations-detail.component';
import { ReservationsManageComponent } from './reservations/reservations-manage/reservations-manage.component';
import { ContactsListComponent } from './contacts/contacts-list/contacts-list.component';
import { ContactsManageComponent } from './contacts/contacts-manage/contacts-manage.component';

const routes: Routes = [
  { path: '', redirectTo: '/reservations/list', pathMatch: 'full'},
  { path: 'reservations/list', component: ReservationsListComponent } ,
  { path: 'reservations/detail/:id', component: ReservationsDetailComponent },
  { path: 'reservations', component: ReservationsManageComponent },
  { path: 'reservations/:id', component: ReservationsManageComponent },
  { path: 'contacts/list', component: ContactsListComponent } ,
  { path: 'contacts', component: ContactsManageComponent },
  { path: 'contacts/:id', component: ContactsManageComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
